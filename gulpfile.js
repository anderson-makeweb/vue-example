"use strict";

var autoprefixer = require("gulp-autoprefixer");
var csso = require("gulp-csso");
var del = require("del");
var gulp = require("gulp");
var gutil = require("gulp-util");
var rename = require("gulp-rename");
var htmlmin = require("gulp-htmlmin");
var runSequence = require("run-sequence");
var less = require("gulp-less");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var watch = require("gulp-watch");

// degradation
const AUTOPREFIXER_BROWSERS = [
  "ie >= 10",
  "ie_mob >= 10",
  "ff >= 30",
  "chrome >= 34",
  "safari >= 7",
  "opera >= 23",
  "ios >= 7",
  "android >= 4.4",
  "bb >= 10"
];

// site
var header_css = ["./node_modules/bootstrap/dist/css/bootstrap.css"];

var header_js = ["./node_modules/jquery/dist/jquery.js"];

var footer_css = ["./css/sweetalert.css"];

var footer_js = [
  "./js/sweetalert.min.js",
  "./js/jquery.validate.js",
  "./js/jquery.mask.js",
  "./js/wow.js",
  "./both/js/both.js"
];

// header css
gulp.task("header_css", function() {
  return gulp
    .src(header_css)
    .pipe(autoprefixer({ browsers: AUTOPREFIXER_BROWSERS }))
    .pipe(concat("styles.css"))
    .pipe(csso())
    .pipe(gulp.dest("./dist/css/"));
});

// footer css
gulp.task("footer_css", function() {
  return gulp
    .src(header_css)
    .pipe(autoprefixer({ browsers: AUTOPREFIXER_BROWSERS }))
    .pipe(concat("footer.css"))
    .pipe(csso())
    .pipe(gulp.dest("./dist/"));
});

// header js
gulp.task("header_js", function() {
  return gulp
    .src(header_js)
    .pipe(concat("scripts.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./dist/js/"));
});

// footer js
gulp.task("footer_js", function() {
  return gulp
    .src(footer_js)
    .pipe(concat("footer.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./dist/"));
});

// less
gulp.task("less", function() {
  return gulp
    .src("./assets/css/styles.less")
    .pipe(
      less().on("error", function(err) {
        gutil.log(err);
        this.emit("end");
      })
    )
    .pipe(autoprefixer({ browsers: AUTOPREFIXER_BROWSERS }))
    .pipe(concat("temp_less.css"))
    .pipe(csso())
    .pipe(gulp.dest("./dist/css"));
});

// concat
gulp.task("concat", function() {
  return gulp
    .src(["./dist/css/temp_css.css", "./dist/css/temp_less.css"])
    .pipe(concat("stylesheet.css"))
    .pipe(gulp.dest("./dist/css"));
});

// dump
gulp.task("dump", () => {
  del(["./dist/css/temp_less.css", "./dist/css/temp_css.css"]);
  console.log("dumping...");
});

// pages
gulp.task("pages", function() {
  return gulp
    .src(["./*.html"])
    .pipe(
      htmlmin({
        collapseWhitespace: true,
        removeComments: true
      })
    )
    .pipe(gulp.dest("./dist"));
});

// clean
gulp.task("clean", () => del(["dist"]));

// watch
gulp.task("watch", function() {
  gulp.watch("./src/css/*", function() {
    runSequence("header_css", "header_js");
  });
});

// sequence
gulp.task("default", ["clean"], function() {
  runSequence("header_css", "header_js", "footer_css", "footer_js");
});
